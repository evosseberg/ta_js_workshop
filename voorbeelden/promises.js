'use strict';

function stemming (smilie) {
    const stemmingen = [':)', ':|', ':(', ';)'];
    return new Promise(function(resolve, reject) {
        if (stemmingen.includes(smilie)) {
            resolve(smilie);
        } else {
            reject(new Error('Stemming onbekend :('));
        }
    });
}

stemming(':)').then(function (uitkomst) {
    console.log(uitkomst); // Zal :) op de console printen
})
    .catch(function (error) {
        console.error(error);
    });

