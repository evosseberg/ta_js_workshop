'use strict';

function volledigeNaam (voornaam, achternaam, callback) {
    const samengesteldeNaam = `${voornaam} ${achternaam}`;
    return callback(samengesteldeNaam);
}

// Gebruiken via een `anonymous function`
volledigeNaam('Joost', 'Arends', function (output) {
    console.log(`Hallo ${output}!`);
});

// Gebruiken via een `named function`
function hoi (naam) {
    console.log(`Hallo ${naam}!`);
}

volledigeNaam('Erik', 'Vosseberg', hoi);
