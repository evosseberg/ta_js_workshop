'use strict';

function stemming (smilie) {
    const stemmingen = [':)', ':|', ':(', ';)'];
    return new Promise(function(resolve, reject) {
        if (stemmingen.includes(smilie)) {
            resolve(smilie);
        } else {
            reject(new Error('Stemming onbekend :('));
        }
    });
}

function verwerkStemming (uitkomst) {
    const bericht = `De ingevoerde stemming is ${uitkomst}`;
    return bericht;
}

async function consoleUitkomst () {
    try {
        const stemmingUitkomst = await stemming(':)');
        const bericht = verwerkStemming(stemmingUitkomst);
        console.log(bericht);
    } catch (error) {
        console.error(error.message);
    }
}
consoleUitkomst();
