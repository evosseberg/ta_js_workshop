'use strict';

const jsonString = `{
    "property": "waarde",
    "arrayVoorbeeld": [
        "waarde1",
        "waarde2"
    ],
    "nestedObjectProperty": {
        "property": "waarde"
    } 
}`;

const exampleObject = {
    property: 'waarde',
    arrayVoorbeeld: [
        'waarde1',
        'waarde2'
    ],
    nestedObjectProperty: {
        property: 'waarde'
    } 
};


// JSON string omzetten naar javascript object
const jsonStringToObject = JSON.parse(jsonString);

// Object omzetten naar JSON string
const exampleObjectToString = JSON.stringify(exampleObject);

// En om het rondje compleet te maken het object die van JSON afkwam weer terug naar JSON. Volg je het nog? ;)
const jsonStringToObjectToString = JSON.stringify(jsonStringToObject)

// Nu beide objecten weer zijn omgezet naar een string kunnen we ze vergelijken. 
console.log(exampleObjectToString === jsonStringToObjectToString);