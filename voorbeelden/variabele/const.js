'use strict';

const variabel = 1;

// Dit comment moet een lap code voorstellen
// waardoor je even niet goed hebt gezien
// dat `variabel` al is gedeclareerd en van waarde voorzien.
variabel = 10;

console.log(variabel);

// "Uitzondering" zou een object zijn, hiervan mag je de inhoud wel aanpassen.

const voorbeeldObject = {
    propertyNaam: 'waarde',
    andereProperty: 'nog een waarde',
};

voorbeeldObject.propertyNaam = 'Nieuwe waarde';

console.log(voorbeeldObject);
