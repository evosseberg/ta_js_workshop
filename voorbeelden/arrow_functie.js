'use strict';
// Functie 1 
function voorbeeldFunctie1 () {
    console.log('voorbeeld functie 1');
}

// Functie 2 met arrow notatie
const voorbeeldFunctie2 = () => {
    console.log('voorbeeld functie 2');
};

// Timeout met anoynmous function callback
setTimeout(function() {
    console.log('twee');
}, 3000);

// Timeout met anoynmous function callback in arrow formaat.
setTimeout(() => {
    console.log('twee');
}, 3000);
