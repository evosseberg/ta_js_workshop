'use strict';

function stemming (smilie) {
    const stemmingen = [':|', ':(', ';)'];
    return new Promise(function (resolve, reject) {
        if (stemmingen.includes(smilie)) {
            resolve(smilie);
        } else {
            reject(new Error('Stemming onbekend :('));
        }
    });
}

function verwerkStemming (uitkomst) {
    const bericht = `De ingevoerde stemming is ${uitkomst}`;
    return Promise.resolve(bericht);
}

stemming(':)')
    .then(verwerkStemming)
    .then(function (bericht) {
        console.log(bericht);
    })
    .catch(function (error) {
        console.error('fout', error);
    }).finally(function () {
        console.log('altijd');
    });
